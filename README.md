# BACNet

## What is BACNet

The Internet has become so ubiquitous that we take it for granted, forgetting how much it depends on political goodwill:
Access restrictions are quite common worldwide, be it the “Chinese firewall”, India cutting the Internet for Kashmir, or the Spanish Police seizing the DNS name of the Catalan independence movement.

The goal of the BACNet (BAsel Citizen Net), which builds on the Secure Scuttlebutt communication protocol, is to implement communication means that avoid this political dependency. The BACNet is being developed as part of the lecture Introduction to Internet and Security at the University of Basel.


## Our project

As a group of three students, we set ourselves the task of implementing the frontend of the social graph of BACNet. Our focus was on the visualization of the network in form of a directed graph, but the frontend should also provide the users with further functionalities. These are mainly the options to follow and unfollow people, as well as the updating of their own user information. The ability to customize the look and feel and the values of the graph attributes should also allow users to customize the look of the frontend to their own taste.


## Setup

To install the necessary dependencies, such as the Django framework, we have set up a script to automate this task. It is located in the folder 'FrontEnd'. The following command installs all needed dependencies: ```python setup.py install```

To get the Frontend up and running, please follow these steps:

1. Run the main program located in the folder 07-BackEnd. This way the necessary PCAP files and the JSON file are created.
2. Maneuver to the folder FrontEnd in which the file manage.py is located.
3. Run the  command: ```python manage.py makemigrations``` This will ensure that changes made to the model are made ready for migration into the database.
4. Afterwards run the command: ```python manage.py migrate``` This will apply the changes to the database.
5. Run the following command to start the server: ```python manage.py runserver```. 
6. Once all steps are completed, go to your browser of choice and open http://127.0.0.1:8000/


